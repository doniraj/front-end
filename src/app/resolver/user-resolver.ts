import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import {Injectable} from '@angular/core';
import {User} from '../model/user';
import {UserService} from '../service/user.service';


@Injectable()
export class UserResolver implements Resolve<User> {

  constructor(private userService: UserService, private router: Router) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> {
    let id = route.paramMap.get('userId');

    return this.userService.getUserByIdOrEmail(id).take(1).map(user => {
      if (user) {
        return user;
      } else { // id not found
        this.router.navigate(['home']);
        return null;
      }
    });
  }
}
