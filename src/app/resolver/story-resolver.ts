import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Story} from '../model/story';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import {StoryService} from '../service/story.service';
import {Injectable} from '@angular/core';


@Injectable()
export class StoryResolver implements Resolve<Story> {

  constructor(private storyService: StoryService, private router: Router) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Story> {
    let title = route.paramMap.get('title');

    return this.storyService.getStoryById(title).take(1).map(story => {
      if (story) {
        return story;
      } else { // id not found
        this.router.navigate(['home']);
        return null;
      }
    });
  }
}
