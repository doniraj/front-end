import {BankAccount} from './bank-account';

export class User {
  id: number;
  accName: string;
  accSurname: string;
  email: string;
  activated: boolean;
  bankAccountSet: BankAccount[];
  avatarUrl: string;
  birthday: string;
  phoneNumber: string;
  role: string;

}
