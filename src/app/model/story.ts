import {User} from './user';

export class Story {
  id: number;
  title: string;
  story: string;
  funds: number;
  dateCreated: Date;
  count: number;
  imageUrl: string;
  pending: boolean;
  user: User;
  deadline: string;
}
