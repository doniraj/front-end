import {Component, OnInit} from '@angular/core';
import {StoryService} from '../service/story.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Story} from '../model/story';
import {AppComponent} from '../app.component';
import {isUndefined} from 'util';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-story',
  templateUrl: './story.component.html',
  styleUrls: ['./story.component.css']
})
export class StoryComponent implements OnInit {

  story: Story;

  closeResult: string;

  constructor(private storyService: StoryService,
              private route: ActivatedRoute,
              private appComponent: AppComponent,
              private router: Router,
              private modalService: NgbModal) {
  }


  ngOnInit() {
    this.getStory();
    console.log(this.story);
    console.log(this.appComponent.user);
  }

  getStory(): void {
    this.route.data.subscribe((data: { story: Story }) => {
        this.story = data.story;
      },
      (error) => {

      }
    );
  }

  isStoryUserLoggedIn(): boolean {
    if (!isUndefined(this.appComponent.user) && this.appComponent.user.id == this.story.user.id) return true;
    return false;
  }

  isLoggedUserAdmin(): boolean {
    if (!isUndefined(this.appComponent.user) && this.appComponent.user.role == 'ROLE_ADMIN') return true;
    return false;
  }

  approveStory(): void {
    this.storyService.postApproveStory(this.story.id).subscribe(() => {
        this.router.navigate(['home']);

      },
      (error) => {

      });
  }

  deleteStory(): void {
    this.storyService.deleteStory(this.story.id).subscribe(() => {
        this.router.navigate(['home']);
      },
      (error) => {

      });
  }

  openModal(modalContent): void {
    this.modalService.open(modalContent).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissedReason(reason)}`;
    });
  }

  private getDismissedReason(reason: any): string {
    if (reason == ModalDismissReasons.ESC) {
      return 'By pressing ESC';
    } else if (reason == ModalDismissReasons.BACKDROP_CLICK) {
      return 'By clicking on a backdrop';
    } else {
      return `With: ${reason}`;
    }
  }


}
