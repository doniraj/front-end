import {Component, OnInit} from '@angular/core';
import {Story} from '../model/story';
import {StoryService} from '../service/story.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AppComponent} from '../app.component';
import {isUndefined} from 'util';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalDismissReasons, NgbDateParserFormatter, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-edit-story',
  templateUrl: './edit-story.component.html',
  styleUrls: ['./edit-story.component.css']
})
export class EditStoryComponent implements OnInit {

  story: Story;
  editStoryFormGroup: FormGroup;
  imageUrl: string;
  closeResult: string;


  constructor(private fb: FormBuilder, private storyService: StoryService, private route: ActivatedRoute, private appComponent: AppComponent,
              private router: Router, private modalService: NgbModal, private ngbDateParserFormatter: NgbDateParserFormatter) {
  }


  ngOnInit() {
    console.log(this.appComponent.user);
    this.getStory();
    if (!this.isLoggedUserAdmin() && !this.isStoryUserLoggedIn()) {
      this.router.navigate(['home']);
    }
    this.imageUrl = this.story.imageUrl;
    this.createForm();
    this.setFormValues();

  }

  getStory(): void {
    this.route.data.subscribe((data: { story: Story }) => {
        this.story = data.story;
      },
      (error) => {

      }
    );
  }

  createForm() {
    this.editStoryFormGroup = this.fb.group(
      {
        title: ['', [Validators.required]],
        imageUrl: ['', [Validators.required]],
        funds: ['', [Validators.required]],
        deadline: [null, []],
        storyDescription: ['']
      });

  }

  setFormValues() {
    this.editStoryFormGroup.setValue({
      title: this.story.title,
      imageUrl: this.story.imageUrl,
      funds: this.story.funds,
      storyDescription: this.story.story,
      deadline: this.ngbDateParserFormatter.parse(this.story.deadline)
    });
  }

  isLoggedUserAdmin(): boolean {
    return !isUndefined(this.appComponent.user) && this.appComponent.user.role == 'ROLE_ADMIN';
  }

  isStoryUserLoggedIn(): boolean {
    return !isUndefined(this.appComponent.user) && this.appComponent.user.id == this.story.user.id;

  }

  editStory() {
    this.story.title = this.editStoryFormGroup.value.title as string;
    this.story.imageUrl = this.editStoryFormGroup.value.imageUrl as string;
    this.story.funds = this.editStoryFormGroup.value.funds as number;
    this.story.deadline = this.ngbDateParserFormatter.format(this.editStoryFormGroup.value.deadline);
    this.story.story = this.editStoryFormGroup.value.storyDescription as string;

    this.storyService.updateStory(this.story).subscribe(() => {
      console.log('Vashata storija e promeneta i pratena vo centarot za proverka na storii. Ke bide prikachena vednash po odobruvanje na istata.');
      this.router.navigate(['home']);


    }, (error) => {

    });

  }

  openModal(modalContent): void {
    this.modalService.open(modalContent).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissedReason(reason)}`;
    });
  }

  private getDismissedReason(reason: any): string {
    if (reason == ModalDismissReasons.ESC) {
      return 'By pressing ESC';
    } else if (reason == ModalDismissReasons.BACKDROP_CLICK) {
      return 'By clicking on a backdrop';
    } else {
      return `With: ${reason}`;
    }
  }

}
