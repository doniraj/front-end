import {Component, OnInit} from '@angular/core';
import {StoryService} from '../service/story.service';
import {CookieService} from 'ngx-cookie-service';
import {Story} from '../model/story';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  stories: Story[];

  constructor(private storyService: StoryService, private cookieService: CookieService) {
    this.stories = [];
  }

  ngOnInit() {
    this.getAllStories();
  }

  getAllStories(): void {
    this.storyService.getAllStories().subscribe((stories) => {
      this.stories = stories;
    });
  }

}
