import {Component, OnInit} from '@angular/core';
import {AppComponent} from "../app.component";
import {ActivatedRoute, Router} from "@angular/router";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {CookieService} from "ngx-cookie-service";
import {UserService} from "../service/user.service";
import {Jwt} from "../model/jwt";
import 'rxjs/add/operator/switchMap';


@Component({
  selector: 'app-activate-user',
  templateUrl: './activate-user.component.html',
  styleUrls: ['./activate-user.component.css']
})
export class ActivateUserComponent implements OnInit {

  token: string;

  constructor(private userService: UserService,
              private route: ActivatedRoute,
              private appComponent: AppComponent,
              private router: Router,
              private modalService: NgbModal,
              private cookieService: CookieService) {

    this.route.params.subscribe(params =>
      this.userService.activateUser(params['token'])
        .subscribe(jwt => {
          console.log(jwt);
          this.activationSuccessful(jwt);
        })
    );

  }


  ngOnInit() {
  }

  activationSuccessful(token: Jwt) {
    const bToken = `Bearer ${token.token}`;
    this.cookieService.set('token', bToken);
    this.appComponent.onLogin();
    this.router.navigate(['home']);
  }

}
