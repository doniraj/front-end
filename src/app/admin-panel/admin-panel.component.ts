import {Component, OnInit} from '@angular/core';
import {AppComponent} from '../app.component';
import {isUndefined} from 'util';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit {

  showingUsers: boolean;

  constructor(private appComponent: AppComponent, private router: Router) {
  }

  ngOnInit() {
    if (!this.checkIfUserLoggedAndAdmin()) {
      this.router.navigate(['home']);
    } else {
      this.showingUsers = true;
    }
  }

  showUsers(): void {
    this.showingUsers = true;
  }

  showStories(): void {
    this.showingUsers = false;
  }

  checkIfUserLoggedAndAdmin(): boolean {
    if (isUndefined(this.appComponent.user) || this.appComponent.user.role == 'ROLE_USER') return false;
    return true;
  }

}
