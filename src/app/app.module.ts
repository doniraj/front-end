import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


import {AppComponent} from './app.component';

import {HomeComponent} from './home/home.component';
import {StoryComponent} from './story/story.component';
import {ProfileComponent} from './profile/profile.component';
import {SignInComponent} from './sign-in/sign-in.component';
import {NewStoryComponent} from './new-story/new-story.component';
import {EditStoryComponent} from './edit-story/edit-story.component';
import {AdminPanelComponent} from './admin-panel/admin-panel.component';
import {UserService} from './service/user.service';
import {StoryService} from './service/story.service';
import {StoryResolver} from './resolver/story-resolver';
import {MyProfileComponent} from './my-profile/my-profile.component';
import {UserResolver} from './resolver/user-resolver';
import {FooterComponent} from './footer/footer.component';
import {AboutUsComponent} from './about-us/about-us.component';
import {ContactComponent} from './contact/contact.component';
import {AdminPanelUsersComponent} from './admin-panel-users/admin-panel-users.component';
import {AdminPanelStoriesComponent} from './admin-panel-stories/admin-panel-stories.component';
import {ContactService} from "./service/contact.service";
import {ActivateUserComponent} from './activate-user/activate-user.component';


const appRoutes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'story/:title', component: StoryComponent, resolve: {story: StoryResolver}},
  {path: 'profile/:userId', component: ProfileComponent, resolve: {user: UserResolver}},
  {path: 'my-profile', component: MyProfileComponent},
  {path: 'sign-in', component: SignInComponent},
  {path: 'story-new', component: NewStoryComponent},
  {path: 'edit-story/:title', component: EditStoryComponent, resolve: {story: StoryResolver}},
  {path: 'admin-panel', component: AdminPanelComponent},
  {path: 'about-us', component: AboutUsComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'activate/:token', component: ActivateUserComponent}

];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    StoryComponent,
    ProfileComponent,
    SignInComponent,
    NewStoryComponent,
    EditStoryComponent,
    AdminPanelComponent,
    MyProfileComponent,
    FooterComponent,
    AboutUsComponent,
    ContactComponent,
    MyProfileComponent,
    AdminPanelUsersComponent,
    AdminPanelStoriesComponent,
    ActivateUserComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: true} // <-- debugging purposes only
    ),
    NgbModule.forRoot()
  ],
  providers: [CookieService, UserService, StoryService, StoryResolver, UserResolver, ContactService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
