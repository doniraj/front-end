import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../service/user.service';
import {CookieService} from 'ngx-cookie-service';
import {AppComponent} from '../app.component';
import {BankAccount} from '../model/bank-account';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  signInFormGroup: FormGroup;
  registerFormGroup: FormGroup;
  bankAccountFormGroup: FormGroup;
  bankAccountSet: BankAccount[];

  showingLogin: boolean;

  constructor(private _location: Location, private fb: FormBuilder, private userService: UserService,
              private cookieService: CookieService, private appComponent: AppComponent,
              private router: Router, private ngbDateParserFormatter: NgbDateParserFormatter) {
    this.bankAccountSet = [];
    this.showingLogin = true;
  }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.signInFormGroup = this.fb.group(
      {
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required]],
      }
    );

    this.registerFormGroup = this.fb.group(
      {
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required]],
        accName: ['', [Validators.required]],
        accSurname: ['', [Validators.required]],
        age: ['', [Validators.required]],
        avatarUrl: [''],
        phoneNumber: [''],
        birthday: [null, []]
      }
    );

    this.bankAccountFormGroup = this.fb.group(
      {
        bankAccountNumber: ['']
      }
    );
  }

  login() {
    const email = this.signInFormGroup.value.email as string;
    const password = this.signInFormGroup.value.password as string;
    this.userService.loginUser(email, password).subscribe((token) => {
        const bToken = `Bearer ${token.token}`;
        this.cookieService.set('token', bToken);
        this.appComponent.onLogin();
        this.router.navigate(['home']);
      },
      (error) => {

      });

  }

  register() {
    const email = this.registerFormGroup.value.email as string;
    const password = this.registerFormGroup.value.password as string;
    const accName = this.registerFormGroup.value.accName as string;
    const accSurname = this.registerFormGroup.value.accSurname as string;
    const phoneNumber = this.registerFormGroup.value.phoneNumber as string;
    const avatarUrl = this.registerFormGroup.value.avatarUrl as string;
    const birthday = this.ngbDateParserFormatter.format(this.registerFormGroup.value.birthday);

    this.userService.addUser(email, password, accName, accSurname, birthday, this.bankAccountSet, avatarUrl, phoneNumber)
      .subscribe((user) => {
          alert("Отворете ја вашата електронска пошта (е-маил) за да го активирате вашиот профил");
        },
        (error) => {

        });
  }

  addBankAccount() {
    const bankAccount = new BankAccount();
    bankAccount.accountNumber = this.bankAccountFormGroup.value.bankAccountNumber as string;
    this.bankAccountSet.push(bankAccount);
  }

  deleteBankAccount(bankAccount: BankAccount) {
    this.bankAccountSet = this.bankAccountSet.filter(ba => ba !== bankAccount);
  }

  showLogin(): void {
    this.showingLogin = true;
  }

  showRegistration(): void {
    this.showingLogin = false;
  }

}
