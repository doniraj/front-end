import {Component, OnInit} from '@angular/core';
import {UserService} from '../service/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AppComponent} from '../app.component';
import {User} from '../model/user';
import {StoryService} from '../service/story.service';
import {Story} from '../model/story';
import {isUndefined} from 'util';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BankAccount} from '../model/bank-account';
import {ModalDismissReasons, NgbDateParserFormatter, NgbDatepickerConfig, NgbModal} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})


export class ProfileComponent implements OnInit {

  ROLE_USER: string = 'ROLE_USER';
  ROLE_ADMIN: string = 'ROLE_ADMIN';

  user: User;
  stories: Story[];
  bankAccountSet: BankAccount[];
  profileFormGroup: FormGroup;

  closeResult: string;
  bankAccountForDeletion: BankAccount;
  storyForDeletion: Story;

  constructor(private userService: UserService, private storyService: StoryService,
              private router: Router, private route: ActivatedRoute, private fb: FormBuilder,
              private appComponent: AppComponent, private modalService: NgbModal,
              private ngbDateParserFormatter: NgbDateParserFormatter, private config: NgbDatepickerConfig) {
    this.stories = [];
    this.bankAccountSet = [];

    config.minDate = {year: 1900, month: 1, day: 1};
    config.maxDate = {year: 2099, month: 12, day: 31};
  }

  ngOnInit() {
    if (!this.checkIfLoggedUserIsAdmin()) {
      this.router.navigate(['home']);
    } else {
      this.getUserAndBankAccounts();
      this.getStories();
      this.createForm();
      this.setFormValues();
    }
  }

  createForm(): void {
    this.profileFormGroup = this.fb.group({
      id: [''],
      email: ['', [Validators.required, Validators.email]],
      name: ['', [Validators.required]],
      surname: ['', [Validators.required]],
      activated: [''],
      avatarUrl: [''],
      birthday: [null, []],
      phone: [''],
      role: ['']
    });
  }

  setFormValues(): void {
    this.profileFormGroup.setValue({
      id: this.user.id,
      email: this.user.email,
      name: this.user.accName,
      surname: this.user.accSurname,
      activated: this.user.activated,
      avatarUrl: this.user.avatarUrl,
      birthday: this.ngbDateParserFormatter.parse(this.user.birthday),
      phone: this.user.phoneNumber,
      role: this.user.role
    });
  }

  getUserAndBankAccounts(): void {
    this.route.data.subscribe((data: { user: User }) => {
      this.user = data.user;
      this.bankAccountSet = data.user.bankAccountSet;
    }, (error) => {

    });
  }

  getStories(): void {
    this.storyService.getAllStoriesByUserId(this.user.id).subscribe((stories) => {
      this.stories = stories;
    }, (error) => {

    });
  }

  editProfile(): void {
    const email = this.profileFormGroup.value.email as string;
    const name = this.profileFormGroup.value.name as string;
    const surname = this.profileFormGroup.value.surname as string;
    const activated = this.profileFormGroup.value.activated as boolean;
    const avatarUrl = this.profileFormGroup.value.avatarUrl as string;
    const birthday = this.ngbDateParserFormatter.format(this.profileFormGroup.value.birthday);
    const phoneNumber = this.profileFormGroup.value.phone as string;
    const role = this.profileFormGroup.value.role as string;

    const user: User = JSON.parse(JSON.stringify(this.user));
    user.email = email;
    user.accName = name;
    user.accSurname = surname;
    user.activated = activated;
    user.avatarUrl = avatarUrl;
    user.birthday = birthday;
    user.phoneNumber = phoneNumber;
    user.role = role;


    this.userService.updateUser(user).subscribe(() => {

    }, (error) => {
    });
  }

  deleteUser(): void {
    this.userService.deleteUser(this.user.id).subscribe(() => {
      this.router.navigate(['home']);
    }, (error) => {

    });
  }

  deleteBankAccount(bankAccount: BankAccount): void {
    this.userService.deleteBankAccount(bankAccount.id).subscribe(() => {
        this.bankAccountSet = this.bankAccountSet.filter(ba => ba !== bankAccount);
      },
      (error) => {

      });
  }

  deleteStory(story: Story): void {
    this.storyService.deleteStory(story.id).subscribe(() => {
      this.stories = this.stories.filter(s => s !== story);
    }, (error) => {

    });
  }

  checkIfLoggedUserIsAdmin(): boolean {
    if (isUndefined(this.appComponent.user) || this.appComponent.user.role == 'ROLE_USER') return false;
    else if (this.appComponent.user.role == 'ROLE_ADMIN') return true;
  }

  openModal(modalContent, story?: Story, bankAccount?: BankAccount): void {

    if (!isUndefined(story)) this.storyForDeletion = story;
    if (!isUndefined(bankAccount)) this.bankAccountForDeletion = bankAccount;

    this.modalService.open(modalContent).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissedReason(reason)}`;
    });
  }

  private getDismissedReason(reason: any): string {
    if (reason == ModalDismissReasons.ESC) {
      return 'By pressing ESC';
    } else if (reason == ModalDismissReasons.BACKDROP_CLICK) {
      return 'By clicking on a backdrop';
    } else {
      return `With: ${reason}`;
    }
  }









}
