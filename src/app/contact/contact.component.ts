import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {AppComponent} from "../app.component";
import {ContactService} from "../service/contact.service";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  contactFormGroup: FormGroup;

  constructor(private fb: FormBuilder,
              private contactService: ContactService,
              private route: ActivatedRoute,
              private appComponent: AppComponent,
              private router: Router,
              private modalService: NgbModal) {
  }

  ngOnInit() {
    this.createForm();
  }

  sendContact() {
    const email = this.contactFormGroup.value.email as string;
    const name = this.contactFormGroup.value.name as string;
    const phone = this.contactFormGroup.value.phone as string;
    const message = this.contactFormGroup.value.message as string;
    console.log(name, email, phone, message);

    this.contactService.sendContact(name, email, phone, message).subscribe(() => {
      console.log('Kontaktot e vospostaven, poradi kontrola na kvalitetot vashiot razgovor kje' +
        ' bide snimen.');
      // TODO: tell user if contact sent successfully
      this.router.navigate(['home']);
    }, (error) => {

    });
  }

  private createForm() {
    this.contactFormGroup = this.fb.group(
      {
        email: ['', [Validators.required]],
        name: ['', [Validators.required]],
        message: ['', [Validators.required]],
        phone: [''],
      });
  }

}
