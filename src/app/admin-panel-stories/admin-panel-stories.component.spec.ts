import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AdminPanelStoriesComponent} from './admin-panel-stories.component';

describe('AdminPanelStoriesComponent', () => {
  let component: AdminPanelStoriesComponent;
  let fixture: ComponentFixture<AdminPanelStoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminPanelStoriesComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPanelStoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
