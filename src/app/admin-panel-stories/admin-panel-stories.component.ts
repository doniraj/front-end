import {Component, OnInit} from '@angular/core';
import {StoryService} from '../service/story.service';
import {Story} from '../model/story';

import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-admin-panel-stories',
  templateUrl: './admin-panel-stories.component.html',
  styleUrls: ['./admin-panel-stories.component.css']
})
export class AdminPanelStoriesComponent implements OnInit {

  PENDING: string = 'PENDING';
  APPROVED: string = 'APPROVED';

  stories: Story[];

  closeResult: string;
  storyIdForDeletion: number;

  constructor(private storyService: StoryService, private modalService: NgbModal) {
    this.stories = [];
  }

  ngOnInit() {
    this.getStories(this.APPROVED);
  }

  getStories(type: string, orderBy?: string): void {
    this.stories = [];
    if (type == this.APPROVED) {
      this.storyService.getAllStories(orderBy).subscribe((stories) => {
        this.stories = stories;
      }, (error) => {

      });
    } else {
      this.storyService.getAllPendingStories(orderBy).subscribe((stories) => {
        this.stories = stories;
      }, (error) => {

      });
    }
  }

  deleteStory(id: number): void {
    this.storyService.deleteStory(id).subscribe(() => {
      this.stories = this.stories.filter(s => s.id !== id);
    }, (error) => {

    });
  }

  openModal(modalContent, storyId: number): void {
    this.storyIdForDeletion = storyId;
    this.modalService.open(modalContent).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissedReason(reason)}`;
    });
  }

  private getDismissedReason(reason: any): string {
    if (reason == ModalDismissReasons.ESC) {
      return 'By pressing ESC';
    } else if (reason == ModalDismissReasons.BACKDROP_CLICK) {
      return 'By clicking on a backdrop';
    } else {
      return `With: ${reason}`;
    }
  }

}
