import {Component, OnInit} from '@angular/core';
import {UserService} from '../service/user.service';
import {StoryService} from '../service/story.service';
import {AppComponent} from '../app.component';
import {User} from '../model/user';
import {Story} from '../model/story';
import {isUndefined} from 'util';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BankAccount} from '../model/bank-account';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.css']
})
export class MyProfileComponent implements OnInit {

  myProfileFormGroup: FormGroup;
  myBankAccountsFormGroup: FormGroup;
  myAvatarFromGroup: FormGroup;
  stories: Story[];
  user: User;
  bankAccountSet: BankAccount[];

  closeResult: string;
  bankAccountForDeletion: BankAccount;

  constructor(private userService: UserService, private storyService: StoryService, private appComponent: AppComponent,
              private router: Router, private fb: FormBuilder, private modalService: NgbModal) {
    this.stories = [];
    this.bankAccountSet = [];
  }

  ngOnInit() {
    this.setUser();
    this.createForm();
    this.setFormValues();
  }

  createForm() {
    this.myProfileFormGroup = this.fb.group(
      {
        name: ['', [Validators.required]],
        surname: ['', [Validators.required]],
        phone: [''],
      });

    this.myBankAccountsFormGroup = this.fb.group(
      {
        bankAccountNumber: ['']
      });

    this.myAvatarFromGroup = this.fb.group(
      {
        avatarUrl: ['']
      });
  }

  setFormValues() {
    this.myProfileFormGroup.setValue({
      name: this.user.accName,
      surname: this.user.accSurname,
      phone: this.user.phoneNumber
    });

    this.myAvatarFromGroup.setValue({
      avatarUrl: this.user.avatarUrl
    });
  }


  setUser(): void {
    if (!isUndefined(this.appComponent.user)) {
      this.user = this.appComponent.user;

      /*
      if(this.user.role == "ROLE_ADMIN") {
        this.router.navigate([`profile/${this.user.id}`]);
      }
      */

      this.getMyStories();
      this.bankAccountSet = this.user.bankAccountSet;
    }
    else {
      this.router.navigate(['sign-in']);
    }

  }

  getMyStories(): void {
    this.storyService.getMyStories().subscribe((stories) => {
        this.stories = stories;
      },
      (error) => {

      });
  }

  editProfile(): void {
    const name = this.myProfileFormGroup.value.name as string;
    const surname = this.myProfileFormGroup.value.surname as string;
    const phone = this.myProfileFormGroup.value.phone as string;
    const user: User = JSON.parse(JSON.stringify(this.user));
    user.accName = name;
    user.accSurname = surname;
    user.phoneNumber = phone;
    user.bankAccountSet = undefined;

    this.userService.updateUser(user).subscribe(() => {

      },
      (error) => {

      });
  }

  addBankAccount() {
    const bankAccount = new BankAccount();
    bankAccount.accountNumber = this.myBankAccountsFormGroup.value.bankAccountNumber as string;
    this.userService.addBankAccount(bankAccount).subscribe(() => {
        this.bankAccountSet.push(bankAccount);
      },
      (error) => {

      });


  }

  deleteBankAccount(bankAccount: BankAccount) {

    this.userService.deleteBankAccount(bankAccount.id).subscribe(() => {
        this.bankAccountSet = this.bankAccountSet.filter(ba => ba !== bankAccount);
      },
      (error) => {

      });
  }

  openModal(modalContent, bankAccount?: BankAccount): void {
    if (!isUndefined(bankAccount)) this.bankAccountForDeletion = bankAccount;

    this.modalService.open(modalContent).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissedReason(reason)}`;
    });
  }

  private getDismissedReason(reason: any): string {
    if (reason == ModalDismissReasons.ESC) {
      return 'By pressing ESC';
    } else if (reason == ModalDismissReasons.BACKDROP_CLICK) {
      return 'By clicking on a backdrop';
    } else {
      return `With: ${reason}`;
    }
  }

  updateAvatar(): void {
    this.user.avatarUrl = this.myAvatarFromGroup.value.avatarUrl as string;
  }

  revertAvatar(): void {
    this.myAvatarFromGroup.setValue({
      avatarUrl: this.user.avatarUrl
    });
  }


}
