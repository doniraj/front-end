import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {StoryService} from '../service/story.service';
import {Router} from '@angular/router';
import {AppComponent} from '../app.component';
import {isUndefined} from 'util';
import {ModalDismissReasons, NgbDateParserFormatter, NgbModal} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-new-story',
  templateUrl: './new-story.component.html',
  styleUrls: ['./new-story.component.css']
})
export class NewStoryComponent implements OnInit {

  newStoryFormGroup: FormGroup;
  imageUrl: string;

  closeResult: string;

  constructor(private storyService: StoryService, private fb: FormBuilder, private router: Router, private appComponent: AppComponent,
              private modalService: NgbModal, private ngbDateParserFormatter: NgbDateParserFormatter) {
  }

  ngOnInit() {
    if (isUndefined(this.appComponent.user)) {
      this.router.navigate(['sign-in']);
    }
    this.createForm();
  }

  createForm() {
    this.newStoryFormGroup = this.fb.group(
      {
        title: ['', [Validators.required]],
        imageUrl: ['', [Validators.required]],
        funds: ['', [Validators.required]],
        deadline: [null, []],
        storyDescription: ['']
      });

  }

  submitStory() {
    const title = this.newStoryFormGroup.value.title as string;
    const imageUrl = this.newStoryFormGroup.value.imageUrl as string;
    const funds = this.newStoryFormGroup.value.funds as number;
    const deadline = this.ngbDateParserFormatter.format(this.newStoryFormGroup.value.deadline);
    const story = this.newStoryFormGroup.value.storyDescription as string;
    console.log(title, imageUrl, funds, story);

    this.storyService.addStory(title, funds, story, imageUrl, deadline).subscribe(() => {
      console.log('Vashata storija e pratena vo centarot za proverka na storii. Ke bide prikachena vednash po odobruvanje na istata.');
      this.router.navigate(['home']);
    }, (error) => {

    });

  }

  openModal(modalContent): void {
    this.modalService.open(modalContent).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissedReason(reason)}`;
    });
  }

  private getDismissedReason(reason: any): string {
    if (reason == ModalDismissReasons.ESC) {
      return 'By pressing ESC';
    } else if (reason == ModalDismissReasons.BACKDROP_CLICK) {
      return 'By clicking on a backdrop';
    } else {
      return `With: ${reason}`;
    }
  }

}
