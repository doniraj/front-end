import {Injectable} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {catchError, tap} from 'rxjs/operators';
import {User} from '../model/user';
import {Jwt} from '../model/jwt';
import {BankAccount} from '../model/bank-account';


@Injectable()
export class UserService {

  private usersUrl = 'https://doniraj-api.herokuapp.com/api/users';
  private activateUrl = 'https://doniraj-api.herokuapp.com/api/users/activate';

  constructor(private httpClient: HttpClient, private cookieService: CookieService) {
  }

  getAllUsers(): Observable<User[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.cookieService.get('token')
      })
    };
    return this.httpClient.get<User[]>(this.usersUrl, httpOptions).pipe(tap(users => console.log('fetched users')),
      catchError(this.handleError('getAllUsers', [])));
  }

  getUserByIdOrEmail(id_or_email: string): Observable<User> {
    const url = `${this.usersUrl}/${id_or_email}`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.cookieService.get('token')
      })
    };
    return this.httpClient.get<User>(url, httpOptions).pipe(tap(user => console.log('fetched user: id=${id}')),
      catchError(this.handleError<User>('getUserById id=${id}')));
  }

  requestPasswordReset(email: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.cookieService.get('token')
      })
    };
    const url = `${this.usersUrl}/request-reset-password`;
    return this.httpClient.post(url, email, httpOptions).pipe(tap(_ => console.log('made request for reset password with email: ${email}')),
      catchError(this.handleError<any>('requestPasswordReset email=${email}')));
  }

  addUser(email: string, userPassword: string, accName: string, accSurname: string, birthday: string, bankAccountSet: BankAccount[], avatarUrl: string,
          phoneNumber: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.cookieService.get('token')
      })
    };
    const url = `${this.usersUrl}/register`;
    const bodyJson = {
      'email': email,
      'userPassword': userPassword,
      'accName': accName,
      'accSurname': accSurname,
      'birthday': birthday,
      'bankAccountSet': bankAccountSet,
      'avatarUrl': avatarUrl,
      'phoneNumber': phoneNumber
    };
    const body = JSON.stringify(bodyJson);
    return this.httpClient.post(url, body, httpOptions).pipe(tap(_ => console.log('made request for creating an user')), catchError(this.handleError<any>('add user')));
  }

  updateUser(user: User): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.cookieService.get('token')
      })
    };
    const id = user.id;
    const url = `${this.usersUrl}/${id}`;
    return this.httpClient.post(url, user, httpOptions).pipe(tap(_ => console.log('made request for patching an user')), catchError(this.handleError<any>('patch user')));
  }

  deleteUser(id: number): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.cookieService.get('token')
      })
    };
    const url = `${this.usersUrl}/${id}`;
    return this.httpClient.delete(url, httpOptions).pipe(tap(_ => console.log('made request for deleting an user')), catchError(this.handleError<any>('delete user')));
  }

  deleteBankAccount(bankId: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.cookieService.get('token')
      })
    };
    const url = `${this.usersUrl}/bank-accounts/${bankId}`;
    return this.httpClient.delete(url, httpOptions).pipe(tap(_ => console.log('made request for deleting a bank account')), catchError(this.handleError<any>('delete bank account')));
  }

  addBankAccount(bankAccount: BankAccount): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.cookieService.get('token')
      })
    };
    const url = `${this.usersUrl}/bank-accounts`;
    return this.httpClient.post(url, bankAccount, httpOptions).pipe(
      tap(_ => console.log('made request for adding a bank account')), catchError(this.handleError<any>('add bank account')));
  }

  loginUser(email: string, password: string): Observable<Jwt> {
    const httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    const jsonBody = {'email': email, 'password': password};
    const body = JSON.stringify(jsonBody);
    const url = `${this.usersUrl}/login`;
    return this.httpClient.post(url, body, httpOptions).pipe(
      tap(_ => console.log('made request for login')), catchError(this.handleError<any>('login user')));
  }

  activateUser(token: string): Observable<Jwt> {
    const url = `${this.activateUrl}/${token}`;
    return this.httpClient.get<Jwt>(url).pipe(
      tap(t => console.log(`Made request to activate user with ${t}`)),
      catchError(this.handleError<Jwt>(''))); // TODO: wtf?
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
