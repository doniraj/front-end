import {Injectable} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {catchError, tap} from 'rxjs/operators';
import {Story} from '../model/story';
import {isUndefined} from 'util';

@Injectable()
export class StoryService {

  private storiesUrl = 'https://doniraj-api.herokuapp.com/api/stories';

  constructor(private httpClient: HttpClient, private cookieService: CookieService) {
  }

  getAllStories(orderBy?: string): Observable<Story[]> {
    if (isUndefined(orderBy)) {
      orderBy = '';
    }
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.cookieService.get('token'),
        'orderBy': orderBy.toString()
      })
    };
    return this.httpClient.get<Story[]>(this.storiesUrl, httpOptions).pipe(tap(stories => console.log('fetched stories')),
      catchError(this.handleError('getAllStories', [])));
  }

  getStoryById(id_or_title: string): Observable<Story> {
    const url = `${this.storiesUrl}/${id_or_title}`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.cookieService.get('token')
      })
    };
    return this.httpClient.get<Story>(url, httpOptions).pipe(tap(story => console.log('fetched story: id=${id}')),
      catchError(this.handleError<Story>('getStoryById id=${id}')));
  }

  postApproveStory(storyId: number): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.cookieService.get('token')
      })
    };
    const url = `${this.storiesUrl}/approve/${storyId}`;
    return this.httpClient.post<any>(url, '', httpOptions).pipe(tap(_ => console.log('approved story')),
      catchError(this.handleError<any>('postApproveStory')));
  }

  addStory(title: string, funds: number, story: string, imageUrl: string, deadline: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.cookieService.get('token')
      })
    };
    const url = `${this.storiesUrl}/new`;
    const bodyJson = {
      'title': title,
      'funds': funds,
      'story': story,
      'imageUrl': imageUrl,
      'deadline': deadline
    };
    const body = JSON.stringify(bodyJson);
    return this.httpClient.post(url, body, httpOptions).pipe(tap(_ => console.log('made request for creating a story')), catchError(this.handleError<any>('add story')));
  }

  getAllPendingStories(orderBy?: string): Observable<Story[]> {
    if (isUndefined(orderBy)) {
      orderBy = '';
    }
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.cookieService.get('token'),
        'orderBy': orderBy.toString()
      })
    };
    const url = `${this.storiesUrl}/pending`;
    return this.httpClient.get<Story[]>(url, httpOptions).pipe(tap(stories => console.log('fetched pending stories')),
      catchError(this.handleError('getAllPendingStories', [])));
  }

  postRejectStory(storyId: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.cookieService.get('token')
      })
    };
    const url = `${this.storiesUrl}/reject/${storyId}`;
    return this.httpClient.post<any>(url, '', httpOptions).pipe(tap(_ => console.log('rejected story')),
      catchError(this.handleError<any>('postRejectStory')));
  }

  getAllStoriesByUserId(userId: number): Observable<Story[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.cookieService.get('token')
      })
    };
    const url = `${this.storiesUrl}/user/${userId}`;
    return this.httpClient.get<Story[]>(url, httpOptions).pipe(tap(stories => console.log('fetched stories by userid')),
      catchError(this.handleError('getAllStoriesByUserId', [])));
  }

  getMyStories(): Observable<Story[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.cookieService.get('token')
      })
    };
    const url = `${this.storiesUrl}/my`;
    return this.httpClient.get<Story[]>(url, httpOptions).pipe(tap(stories => console.log('fetched my stories')),
      catchError(this.handleError('getMyStories', [])));
  }

  updateStory(story: Story): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.cookieService.get('token')
      })
    };
    const id = story.id;
    const url = `${this.storiesUrl}/${id}`;
    return this.httpClient.post(url, story, httpOptions).pipe(tap(_ => console.log('made request for patching a story')), catchError(this.handleError<any>('patch story')));
  }

  deleteStory(id: number): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.cookieService.get('token')
      })
    };
    const url = `${this.storiesUrl}/${id}`;
    return this.httpClient.delete(url, httpOptions).pipe(tap(_ => console.log('made request for deleting a story')), catchError(this.handleError<any>('delete story')));
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
