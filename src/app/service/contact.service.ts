import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Observable";
import {catchError, tap} from "rxjs/operators";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {of} from "rxjs/observable/of";

@Injectable()
export class ContactService {

  contactUrl = "https://doniraj-api.herokuapp.com/contact";

  constructor(private httpClient: HttpClient) {
  }

  sendContact(name: string, email: string, phone: string, message: string): Observable<any> {
    const url = `${this.contactUrl}`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    const bodyJson = {
      'name': name,
      'email': email,
      'phone': phone,
      'message': message,
    };

    console.log(bodyJson);

    const body = JSON.stringify(bodyJson);
    return this.httpClient.post(url, body, httpOptions)
      .pipe(
        tap(_ => console.log('made request for contact')),
        catchError(this.handleError<any>('send contact')));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
