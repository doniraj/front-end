import {Component, OnInit} from '@angular/core';
import {User} from './model/user';
import {CookieService} from 'ngx-cookie-service';
import {UserService} from './service/user.service';
import {isUndefined} from 'util';
import * as decode from 'jwt-decode';
import {NavigationEnd, Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'Doniraj.MK';
  user: User;
  loadAppComponent: boolean;

  constructor(private cookieService: CookieService, private userService: UserService, private router: Router) {
  }

  ngOnInit(): void {
    this.loadAppComponent = false;
    this.checkUser();
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      try {
        window.scrollTo({left: 0, top: 0, behavior: 'smooth'});
      }
      catch (e) {
        window.scrollTo(0, 0);
      }
    });
  }

  checkUser(): void {
    if (isUndefined(this.user)) {
      this.checkForToken();
    }
  }

  checkForToken(): void {
    if (this.cookieService.check('token')) {
      const token = this.cookieService.get('token');
      const decoded_token = decode(token);
      this.userService.getUserByIdOrEmail(decoded_token.sub).subscribe((user) => {
          this.user = user;
          this.loadAppComponent = true;
          console.log('set to true');
        },
        (error) => {
          this.loadAppComponent = true;
        });
    } else {
      this.loadAppComponent = true;
    }

  }

  onLogin(): void {
    this.checkUser();
  }

  isLoggedUserAdmin(): boolean {
    if (!isUndefined(this.user) && this.user.role == 'ROLE_ADMIN') return true;
    return false;
  }

  logout(): void {
    this.user = undefined;
    this.cookieService.delete('token');
    location.reload();
  }
}
